package com.example.task.web.rest;

import com.example.task.service.QuestionService;
import com.example.task.service.dto.question.request.create.QuestionCreateRequestDto;
import com.example.task.service.dto.question.response.admin.QuestionAdminResponseDto;
import com.example.task.service.dto.question.response.user.QuestionResponseDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Validated
@RestController
@RequiredArgsConstructor
@RequestMapping("/question")
public class QuestionController {

    private final QuestionService questionService;

    @PostMapping
    public ResponseEntity<QuestionResponseDto> create(
            @RequestBody @Valid QuestionCreateRequestDto requestDto) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(questionService.create(requestDto));
    }

    @GetMapping("/admin/{id}")
    public ResponseEntity<QuestionAdminResponseDto> getAsAdmin(@PathVariable Long id) {
        return ResponseEntity.ok(questionService.getAsAdmin(id));
    }

    @GetMapping("/user/{id}")
    public ResponseEntity<QuestionResponseDto> get(@PathVariable Long id) {
        return ResponseEntity.ok(questionService.getById(id));
    }

    @PutMapping("/admin/{id}")
    public ResponseEntity<QuestionAdminResponseDto> updateAsAdmin(@PathVariable Long id,
                                                                  @RequestBody @Valid QuestionCreateRequestDto requestDto) {
        return ResponseEntity.ok(questionService.updateAsAdmin(id, requestDto));
    }

    @PutMapping("/user/{id}")
    public ResponseEntity<QuestionResponseDto> update(@PathVariable Long id,
                                                      @RequestBody @Valid QuestionCreateRequestDto requestDto) {
        return ResponseEntity.ok(questionService.update(id, requestDto));
    }

    @DeleteMapping("/admin/{id}")
    public void deleteUser(@PathVariable Long id) {
        questionService.delete(id);
    }

}
