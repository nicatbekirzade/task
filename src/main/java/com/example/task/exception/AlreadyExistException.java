package com.example.task.exception;

public class AlreadyExistException extends RuntimeException {

    private static final long serialVersionUID = 58432132465811L;

    public AlreadyExistException(String message) {
        super(message);
    }
}
