package com.example.task.config;

import com.example.task.service.impl.UserDetailsServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private static final String USER = "USER";
    private static final String ADMIN = "ADMIN";
    private static final String QUESTION_USER = "/question/user/**";
    private static final String QUESTION_ADMIN = "/question/admin/**";
    private static final String USER_PATH = "/user**";

    @Bean
    @Override
    public UserDetailsService userDetailsService() {
        return new UserDetailsServiceImpl();
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth
                .userDetailsService(userDetailsService())
                .passwordEncoder(passwordEncoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http
                .httpBasic()
                .and()
                .authorizeRequests()
                .antMatchers(HttpMethod.POST, QUESTION_USER).hasAnyAuthority(USER, ADMIN)
                .antMatchers(HttpMethod.GET, QUESTION_USER).hasAnyAuthority(USER, ADMIN)
                .antMatchers(HttpMethod.PUT, QUESTION_USER).hasAnyAuthority(ADMIN, USER)
                .antMatchers(HttpMethod.GET, QUESTION_ADMIN).hasAuthority(ADMIN)
                .antMatchers(HttpMethod.PUT, QUESTION_ADMIN).hasAuthority(ADMIN)
                .antMatchers(HttpMethod.DELETE, QUESTION_ADMIN).hasAuthority(ADMIN)
                .antMatchers(HttpMethod.DELETE, USER_PATH).hasAuthority(ADMIN)
                .antMatchers(HttpMethod.POST, USER_PATH).hasAnyAuthority(ADMIN, USER)
                .anyRequest().permitAll()
                .and().csrf().disable()
                .headers().frameOptions().disable()
                .and().formLogin().disable();
    }
}
