package com.example.task.dto;

import lombok.Data;

@Data
public class RoleDto {

    private String role;
}
