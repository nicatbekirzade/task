package com.example.task;

import com.example.task.domain.User;
import com.example.task.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Slf4j
@RequiredArgsConstructor
@SpringBootApplication
@Configuration
public class TaskApplication implements CommandLineRunner {

	private final UserRepository userRepository;
	//	private final MyUserDetailService myUserDetailService;
//	private final JwtService jwtService;
	private final BCryptPasswordEncoder passwordEncoder;


		public static void main (String[] args){
		SpringApplication.run(TaskApplication.class, args);
	}

		@Override
		public void run (String...args) throws Exception {
//		User user = new User();
//		user.setPassword(passwordEncoder.encode("1234"));
//		user.setUserName("admin");
//		userRepository.save(user);

//		Claims claims = jwtService.parseToken("eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImlhdCI6MTYxMjYxMjQ0OSwiZXhwIjoxNjEyNjk4ODQ5LCJyb2xlcyI6WyJST0xFX0FETUlOIl19.2ZVTfKGbBvvWW0XQyNOheTgOwFdHM-G6u9lBJItQBqZpAj64-YetX1iUurxnXhh8WQTPky0o1ekcngxcjHgViQ");
//		System.out.println(claims);

	}

}
