package com.example.task.domain.enumeration;

public enum ConfirmationType {

        CONFIRMED, PENDING

}
