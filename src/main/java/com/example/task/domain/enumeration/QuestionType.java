package com.example.task.domain.enumeration;

public enum QuestionType {
    MULTI, SINGLE, FREE_ANSWER
}
