package com.example.task.service.impl;

import com.example.task.domain.User;
import com.example.task.dto.UserDto;
import com.example.task.exception.AlreadyExistException;
import com.example.task.exception.NotFoundException;
import com.example.task.repository.UserRepository;
import com.example.task.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

//    private static final String ADMIN = "ADMIN";
//    private static final String USER = "USER";

    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final UserRepository userRepository;
    private final ModelMapper modelMapper;

    @Override
    public void createUser(UserDto userDto) {
        checkIfUserExistsByUserName(userDto.getUserName());
        checkIfUserExistsByEmail(userDto.getEmail());
        userDto.setPassword(bCryptPasswordEncoder.encode(userDto.getPassword()));
        userRepository.save(modelMapper.map(userDto, User.class));
    }

    @Override
    public void delete(Long id) {
        try {
            userRepository.deleteById(id);
        } catch (EmptyResultDataAccessException ex) {
            throw new NotFoundException(String.format("Entry with id %s not found", id));
        }
    }

    private void checkIfUserExistsByEmail(String email) {
        userRepository.findByEmail(email)
                .ifPresent(u -> {
                    throw new AlreadyExistException(
                            String.format("User by email: '%s' already exists", email));
                });
    }

    private void checkIfUserExistsByUserName(String userName) {
        userRepository.findByUserName(userName).ifPresent(u -> {
            throw new AlreadyExistException(String.format(
                    "User by user name : '%s' already exists", userName));
        });
    }
}
