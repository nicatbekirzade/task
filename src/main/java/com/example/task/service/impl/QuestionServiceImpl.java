package com.example.task.service.impl;

import com.example.task.domain.Question;
import com.example.task.exception.NotFoundException;
import com.example.task.repository.QuestionRepository;
import com.example.task.service.QuestionService;
import com.example.task.service.dto.question.request.create.QuestionCreateRequestDto;
import com.example.task.service.dto.question.response.admin.QuestionAdminResponseDto;
import com.example.task.service.dto.question.response.user.QuestionResponseDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class QuestionServiceImpl implements QuestionService {

    private final QuestionRepository questionRepository;
    private final ModelMapper modelMapper;

    @Override
    public QuestionResponseDto create(QuestionCreateRequestDto questionDto) {
        Question saved = questionRepository.save(modelMapper.map(questionDto, Question.class));
        return modelMapper.map(saved, QuestionResponseDto.class);
    }

    @Override
    public void delete(Long id) {
        //
    }

    @Override
    public QuestionAdminResponseDto getAsAdmin(Long id) {
        Question response = questionRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(String.format("Question with id %s not found", id)));
        return modelMapper.map(response, QuestionAdminResponseDto.class);
    }

    @Override
    public QuestionResponseDto getById(Long id) {
        Question response = questionRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(String.format("Question with id %s not found", id)));
        return modelMapper.map(response, QuestionResponseDto.class);
    }

    @Override
    public QuestionAdminResponseDto updateAsAdmin(Long id, QuestionCreateRequestDto questionDto) {
        return questionRepository.findById(id).map(question -> {
            modelMapper.map(questionDto, question);
            question.setId(id);
            return modelMapper.map(question, QuestionAdminResponseDto.class);
        }).orElseThrow(() -> new NotFoundException(String.format("Question with id %s not found", id)));
    }

    @Override
    public QuestionResponseDto update(Long id, QuestionCreateRequestDto questionDto) {
        return questionRepository.findById(id).map(question -> {
            modelMapper.map(questionDto, question);
            question.setId(id);
            return modelMapper.map(question, QuestionResponseDto.class);
        }).orElseThrow(() -> new NotFoundException(String.format("Question with id %s not found", id)));

    }
}
