package com.example.task.service;

import com.example.task.dto.UserDto;

public interface UserService {

    void createUser(UserDto userDto);

    void delete(Long id);

}
