package com.example.task.service;

import com.example.task.service.dto.question.request.create.QuestionCreateRequestDto;
import com.example.task.service.dto.question.response.admin.QuestionAdminResponseDto;
import com.example.task.service.dto.question.response.user.QuestionResponseDto;

public interface QuestionService {


    QuestionResponseDto create(QuestionCreateRequestDto questionDto);

    void delete(Long id);

    QuestionAdminResponseDto getAsAdmin(Long id);

    QuestionResponseDto getById(Long id);

    QuestionAdminResponseDto updateAsAdmin(Long id, QuestionCreateRequestDto questionDto);

    QuestionResponseDto update(Long id, QuestionCreateRequestDto questionDto);
}
