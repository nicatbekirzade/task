package com.example.task.service.dto;

import lombok.Data;

@Data
public class AssessmentResponseDto {

    private Long id;
    private String name;
    private int dDegree;
    private String description;
}
