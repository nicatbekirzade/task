package com.example.task.service.dto.question.response.admin;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AdminAnswerDto {

    private Long id;
    private String answer;
    private Boolean correct;

}
