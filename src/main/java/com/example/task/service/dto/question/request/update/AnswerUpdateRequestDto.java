package com.example.task.service.dto.question.request.update;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AnswerUpdateRequestDto {

    private Long id;

    @NotBlank
    private String answer;

    @NotNull
    private Boolean correct;

}
