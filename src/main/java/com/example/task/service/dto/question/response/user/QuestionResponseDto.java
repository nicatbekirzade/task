package com.example.task.service.dto.question.response.user;

import com.example.task.domain.enumeration.QuestionType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class QuestionResponseDto {

    private Long id;
    private String question;
    private QuestionType type;
    private Set<UserAnswerDto> answers;

}
