package com.example.task.service.dto.question.request.create;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AnswerCreateRequestDto {

    @NotBlank
    private String answer;

    @NotNull
    private Boolean correct;

}
