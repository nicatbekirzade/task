package com.example.task.service.dto.question.request.update;

import com.example.task.domain.enumeration.QuestionType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class QuestionUpdateRequestDto {

    @NotBlank
    private String question;

    @NotNull
    private QuestionType type;

    @NotNull
    private Set<AnswerUpdateRequestDto> answers;

}
