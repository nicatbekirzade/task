package com.example.task.service.dto.question.response.admin;

import com.example.task.domain.enumeration.QuestionType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class QuestionAdminResponseDto {

    private Long id;
    private String question;
    private QuestionType type;
    private Set<AdminAnswerDto> answers;

}
